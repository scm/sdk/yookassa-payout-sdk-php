<?php

/*
 * The MIT License
 *
 * Copyright (c) 2024 "YooMoney", NBСO LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace YooKassaPayout\Common;

/**
 * Класс объекта ответа, возвращаемого API при запросе синонима карты
 *
 * @package YooKassaPayout
 */
class ResponseSynonymCard
{
    /**
     * Результат обработки данных
     * @var string
     */
    protected $reason;
    /**
     * Код карточного продукта
     * @var string
     */
    protected $skrDestinationCardProductCode;
    /**
     * Синоним банковской карты
     * @var string
     */
    protected $skrDestinationCardSynonim;
    /**
     * Тип банковской системы карты
     * @var string
     */
    protected $skrDestinationCardType;
    /**
     * Первые 6 цифр карты
     * @var string
     */
    protected $skrCardBin;
    /**
     * Последние 4 цифры карты
     * @var string
     */
    protected $skrCardLast4;
    /**
     * Цифровой код страны выпуска карты
     * @var string
     */
    protected $skrDestinationCardCountryCode;
    /**
     * Маска банковской карты
     * @var string
     */
    protected $skrDestinationCardPanmask;

    /**
     * ResponseSynonymCard constructor.
     * @param string $body Строка JSON
     */
    public function __construct($body)
    {
        $properties = json_decode($body, true);
        $this->buildByPropertiesArray($properties['storeCard']);
    }

    /**
     * Заполняет свойства объекта из массива
     * @param array $properties Массив параметров карты
     */
    protected function buildByPropertiesArray($properties)
    {
        foreach ($properties as $propertyName => $value) {
            $parts = array_map(
                function($part) {
                    return ucfirst($part);
                },
                explode('_', $propertyName)
            );

            $propertyKey = lcfirst(implode('', $parts));
            $this->{$propertyKey} = $value;
        }
    }

    /**
     * Возвращает результат обработки данных
     * @return string Результат обработки данных
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * Возвращает первые 6 цифр карты
     * @return string Первые 6 цифр карты
     */
    public function getSkrCardBin()
    {
        return $this->skrCardBin;
    }

    /**
     * Возвращает последние 4 цифры карты
     * @return string Последние 4 цифры карты
     */
    public function getSkrCardLast4()
    {
        return $this->skrCardLast4;
    }

    /**
     * Возвращает цифровой код страны выпуска карты
     * @return string Цифровой код страны выпуска карты
     */
    public function getSkrDestinationCardCountryCode()
    {
        return $this->skrDestinationCardCountryCode;
    }

    /**
     * Возвращает маску банковской карты
     * @return string Маска банковской карты
     */
    public function getSkrDestinationCardPanmask()
    {
        return $this->skrDestinationCardPanmask;
    }

    /**
     * Возвращает код карточного продукта
     * @return string Код карточного продукта
     */
    public function getSkrDestinationCardProductCode()
    {
        return $this->skrDestinationCardProductCode;
    }

    /**
     * Возвращает синоним банковской карты
     * @return string Синоним банковской карты
     */
    public function getSkrDestinationCardSynonim()
    {
        return $this->skrDestinationCardSynonim;
    }

    /**
     * Возвращает тип банковской системы карты
     * @return string Тип банковской системы карты
     */
    public function getSkrDestinationCardType()
    {
        return $this->skrDestinationCardType;
    }
}
